package org.light4admin.modules.system.controller;

import org.light4admin.common.base.BaseController;
import org.light4admin.common.Constants;
import org.light4admin.common.aop.auth.PreAuthorize;
import org.light4admin.common.base.PageResponse;
import org.light4admin.common.base.Response;
import org.light4admin.modules.system.domain.SysConfig;
import org.light4admin.modules.system.service.ISysConfigService;
import org.light4admin.modules.system.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 参数配置 信息操作处理
 *
 * @author light4admin
 */
@RestController
@RequestMapping("/config")
public class SysConfigController extends BaseController {
    @Autowired
    private ISysConfigService configService;

    /**
     * 获取参数配置列表
     */
    @PreAuthorize(hasPermi = "system:config:list")
    @GetMapping("/list")
    public PageResponse list(SysConfig config) {
        startPage();
        List<SysConfig> list = configService.selectConfigList(config);
        return getDataTable(list);
    }


    /**
     * 根据参数编号获取详细信息
     */
    @GetMapping(value = "/{configId}")
    public Response<SysConfig> getInfo(@PathVariable Long configId) {
        return Response.success(configService.selectConfigById(configId));
    }

    /**
     * 根据参数键名查询参数值
     */
    @GetMapping(value = "/configKey/{configKey}")
    public Response<String> getConfigKey(@PathVariable String configKey) {
        return Response.success(configService.selectConfigByKey(configKey));
    }

    /**
     * 新增参数配置
     */
    @PreAuthorize(hasPermi = "system:config:add")
    @PostMapping
    public Response add(@Validated @RequestBody SysConfig config) {
        if (Constants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
            return Response.failed("新增参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        config.setCreateBy(SecurityUtils.getUsername());
        configService.insertConfig(config);
        return Response.success();
    }

    /**
     * 修改参数配置
     */
    @PreAuthorize(hasPermi = "system:config:edit")
    @PutMapping
    public Response<Void> edit(@Validated @RequestBody SysConfig config) {
        if (Constants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
            return Response.failed("修改参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        config.setUpdateBy(SecurityUtils.getUsername());
        configService.updateConfig(config);
        return Response.success();
    }

    /**
     * 删除参数配置
     */
    @PreAuthorize(hasPermi = "system:config:remove")
    @DeleteMapping("/{configIds}")
    public Response remove(@PathVariable Long[] configIds) {
        configService.deleteConfigByIds(configIds);
        return Response.success();
    }

    /**
     * 清空缓存
     */
    @PreAuthorize(hasPermi = "system:config:remove")
    @DeleteMapping("/clearCache")
    public Response clearCache() {
        configService.clearCache();
        return Response.success();
    }
}
