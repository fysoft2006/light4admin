package org.light4admin.modules.system.controller;

import org.light4admin.common.base.BaseController;
import org.light4admin.common.Constants;
import org.light4admin.common.aop.auth.PreAuthorize;
import org.light4admin.common.base.PageResponse;
import org.light4admin.common.base.Response;
import org.light4admin.modules.system.domain.SysRole;
import org.light4admin.modules.system.service.ISysRoleService;
import org.light4admin.modules.system.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色信息
 *
 * @author light4admin
 */
@RestController
@RequestMapping("/role")
public class SysRoleController extends BaseController {
    @Autowired
    private ISysRoleService roleService;

    @PreAuthorize(hasPermi = "system:role:list")
    @GetMapping("/list")
    public PageResponse list(SysRole role) {
        startPage();
        List<SysRole> list = roleService.selectRoleList(role);
        return getDataTable(list);
    }


    /**
     * 根据角色编号获取详细信息
     */
    @PreAuthorize(hasPermi = "system:role:query")
    @GetMapping(value = "/{roleId}")
    public Response<SysRole> getInfo(@PathVariable Long roleId) {
        return Response.success(roleService.selectRoleById(roleId));
    }

    /**
     * 新增角色
     */
    @PreAuthorize(hasPermi = "system:role:add")
    @PostMapping
    public Response<Void> add(@Validated @RequestBody SysRole role) {
        if (Constants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
            return Response.failed("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
        } else if (Constants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
            return Response.failed("新增角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
        role.setCreateBy(SecurityUtils.getUsername());
        roleService.insertRole(role);
        return Response.success();

    }

    /**
     * 修改保存角色
     */
    @PreAuthorize(hasPermi = "system:role:edit")
    @PutMapping
    public Response<Void> edit(@Validated @RequestBody SysRole role) {
        roleService.checkRoleAllowed(role);
        if (Constants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
            return Response.failed("修改角色'" + role.getRoleName() + "'失败，角色名称已存在");
        } else if (Constants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
            return Response.failed("修改角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
        role.setUpdateBy(SecurityUtils.getUsername());
        roleService.updateRole(role);
        return Response.success();
    }

    /**
     * 修改保存数据权限
     */
    @PreAuthorize(hasPermi = "system:role:edit")
    @PutMapping("/dataScope")
    public Response<Void> dataScope(@RequestBody SysRole role) {
        roleService.checkRoleAllowed(role);
        roleService.authDataScope(role);
        return Response.success();
    }

    /**
     * 状态修改
     */
    @PreAuthorize(hasPermi = "system:role:edit")
    @PutMapping("/changeStatus")
    public Response<Void> changeStatus(@RequestBody SysRole role) {
        roleService.checkRoleAllowed(role);
        role.setUpdateBy(SecurityUtils.getUsername());
        roleService.updateRoleStatus(role);
        return Response.success();
    }

    /**
     * 删除角色
     */
    @PreAuthorize(hasPermi = "system:role:remove")
    @DeleteMapping("/{roleIds}")
    public Response<Void> remove(@PathVariable Long[] roleIds) {
        roleService.deleteRoleByIds(roleIds);
        return Response.success();
    }

    /**
     * 获取角色选择框列表
     */
    @PreAuthorize(hasPermi = "system:role:query")
    @GetMapping("/optionselect")
    public Response<List<SysRole>> optionselect() {
        return Response.success(roleService.selectRoleAll());
    }
}
