package org.light4admin.modules.system.contract.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录-请求
 *
 * @author light4admin
 */
@Data
public class SysLoginReq {
    @NotBlank
    @ApiModelProperty("用户名")
    private String username;

    @NotBlank
    @ApiModelProperty("用户密码")
    private String password;

    @NotBlank
    @ApiModelProperty("验证码")
    private String code;

    @NotBlank
    @ApiModelProperty("验证码uuid")
    private String uuid = "";
}
