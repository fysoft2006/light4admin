package org.light4admin.modules.system.utils;

import org.apache.commons.lang.StringUtils;
import org.light4admin.common.CacheConstants;
import org.light4admin.util.ServletUtils;
import org.light4admin.util.text.Convert;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * 权限获取工具类
 *
 * @author light4admin
 */
public class SecurityUtils {
    /**
     * 获取用户
     */
    public static String getUsername() {
        HttpServletRequest request = ServletUtils.getRequest();
        if (Objects.isNull(request)) return null;
        String username = Objects.toString(request.getAttribute(CacheConstants.DETAILS_USERNAME), "");
        return ServletUtils.urlDecode(username);
    }

    /**
     * 获取用户ID
     */
    public static Long getUserId() {
        HttpServletRequest request = ServletUtils.getRequest();
        if (Objects.isNull(request)) return null;
        return Convert.toLong(request.getAttribute(CacheConstants.DETAILS_USER_ID));
    }

    /**
     * 获取请求token
     */
    public static String getToken() {
        return getToken(ServletUtils.getRequest());
    }

    /**
     * 根据request获取请求token
     */
    public static String getToken(HttpServletRequest request) {
        if (Objects.isNull(request)) {
            return "";
        }
        String token = request.getHeader(CacheConstants.HEADER);
        if (StringUtils.isNotEmpty(token) && token.startsWith(CacheConstants.TOKEN_PREFIX)) {
            token = token.replace(CacheConstants.TOKEN_PREFIX, "");
        }
        return token;
    }

    /**
     * 是否为管理员
     *
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdmin(Long userId) {
        return userId != null && 1L == userId;
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword     真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

}
