package org.light4admin.modules.system.controller;

import org.apache.commons.lang.StringUtils;
import org.light4admin.common.base.BaseController;
import org.light4admin.common.Constants;
import org.light4admin.common.aop.auth.PreAuthorize;
import org.light4admin.common.base.PageResponse;
import org.light4admin.common.base.Response;
import org.light4admin.modules.system.domain.SysRole;
import org.light4admin.modules.system.domain.SysUser;
import org.light4admin.modules.system.domain.vo.LoginUser;
import org.light4admin.modules.system.service.ISysPermissionService;
import org.light4admin.modules.system.service.ISysPostService;
import org.light4admin.modules.system.service.ISysRoleService;
import org.light4admin.modules.system.service.ISysUserService;
import org.light4admin.modules.system.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户信息
 *
 * @author light4admin
 */
@RestController
@RequestMapping("/user")
public class SysUserController extends BaseController {
    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysPermissionService permissionService;

    /**
     * 获取用户列表
     */
    @PreAuthorize(hasPermi = "system:user:list")
    @GetMapping("/list")
    public PageResponse list(SysUser user) {
        startPage();
        List<SysUser> list = userService.selectUserList(user);
        return getDataTable(list);
    }

    /**
     * 获取当前用户信息
     */
    @GetMapping("/info/{username}")
    public Response<LoginUser> info(@PathVariable("username") String username) {
        SysUser sysUser = userService.selectUserByUserName(username);
        if (Objects.isNull(sysUser)) {
            return Response.failed("用户名或密码错误");
        }
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(sysUser.getUserId());
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(sysUser.getUserId());
        LoginUser sysUserVo = new LoginUser();
        sysUserVo.setSysUser(sysUser);
        sysUserVo.setRoles(roles);
        sysUserVo.setPermissions(permissions);
        return Response.success(sysUserVo);
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("/getInfo")
    public Response<Map<String, Object>> getInfo() {
        Long userId = SecurityUtils.getUserId();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(userId);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(userId);
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("user", userService.selectUserById(userId));
        dataMap.put("roles", roles);
        dataMap.put("permissions", permissions);
        return Response.success(dataMap);
    }

    /**
     * 根据用户编号获取详细信息
     */
    @PreAuthorize(hasPermi = "system:user:query")
    @GetMapping(value = {"/", "/{userId}"})
    public Response<Map<String, Object>> getInfo(@PathVariable(value = "userId", required = false) Long userId) {
        Map<String, Object> dataMap = new HashMap<>();
        List<SysRole> roles = roleService.selectRoleAll();
        dataMap.put("roles", SysUser.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        dataMap.put("posts", postService.selectPostAll());
        if (Objects.nonNull(userId)) {
            dataMap.put("user", userService.selectUserById(userId));
            dataMap.put("postIds", postService.selectPostListByUserId(userId));
            dataMap.put("roleIds", roleService.selectRoleListByUserId(userId));
        }
        return Response.success(dataMap);
    }

    /**
     * 新增用户
     */
    @PreAuthorize(hasPermi = "system:user:add")
    @PostMapping
    public Response<Void> add(@Validated @RequestBody SysUser user) {
        if (Constants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user.getUserName()))) {
            return Response.failed("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
        } else if (StringUtils.isNotEmpty(user.getPhonenumber())
                && Constants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user))) {
            return Response.failed("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        } else if (StringUtils.isNotEmpty(user.getEmail())
                && Constants.NOT_UNIQUE.equals(userService.checkEmailUnique(user))) {
            return Response.failed("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setCreateBy(SecurityUtils.getUsername());
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        userService.insertUser(user);
        return Response.success();
    }

    /**
     * 修改用户
     */
    @PreAuthorize(hasPermi = "system:user:edit")
    @PutMapping
    public Response<Void> edit(@Validated @RequestBody SysUser user) {
        userService.checkUserAllowed(user);
        if (StringUtils.isNotEmpty(user.getPhonenumber())
                && Constants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user))) {
            return Response.failed("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        } else if (StringUtils.isNotEmpty(user.getEmail())
                && Constants.NOT_UNIQUE.equals(userService.checkEmailUnique(user))) {
            return Response.failed("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setUpdateBy(SecurityUtils.getUsername());
        userService.updateUser(user);
        return Response.success();
    }

    /**
     * 删除用户
     */
    @PreAuthorize(hasPermi = "system:user:remove")
    @DeleteMapping("/{userIds}")
    public Response<Void> remove(@PathVariable Long[] userIds) {
        userService.deleteUserByIds(userIds);
        return Response.success();
    }

    /**
     * 重置密码
     */
    @PreAuthorize(hasPermi = "system:user:edit")
    @PutMapping("/resetPwd")
    public Response<Void> resetPwd(@RequestBody SysUser user) {
        userService.checkUserAllowed(user);
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        user.setUpdateBy(SecurityUtils.getUsername());
        userService.resetPwd(user);
        return Response.success();
    }

    /**
     * 状态修改
     */
    @PreAuthorize(hasPermi = "system:user:edit")
    @PutMapping("/changeStatus")
    public Response<Void> changeStatus(@RequestBody SysUser user) {
        userService.checkUserAllowed(user);
        user.setUpdateBy(SecurityUtils.getUsername());
        userService.updateUserStatus(user);
        return Response.success();
    }

}
