package org.light4admin.modules.system.controller;

import org.light4admin.common.base.BaseController;
import org.light4admin.common.Constants;
import org.light4admin.common.aop.auth.PreAuthorize;
import org.light4admin.common.base.PageResponse;
import org.light4admin.common.base.Response;
import org.light4admin.modules.system.domain.SysPost;
import org.light4admin.modules.system.service.ISysPostService;
import org.light4admin.modules.system.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 岗位信息操作处理
 *
 * @author light4admin
 */
@RestController
@RequestMapping("/post")
public class SysPostController extends BaseController {
    @Autowired
    private ISysPostService postService;

    /**
     * 获取岗位列表
     */
    @PreAuthorize(hasPermi = "system:post:list")
    @GetMapping("/list")
    public PageResponse list(SysPost post) {
        startPage();
        List<SysPost> list = postService.selectPostList(post);
        return getDataTable(list);
    }


    /**
     * 根据岗位编号获取详细信息
     */
    @PreAuthorize(hasPermi = "system:post:query")
    @GetMapping(value = "/{postId}")
    public Response<SysPost> getInfo(@PathVariable Long postId) {
        return Response.success(postService.selectPostById(postId));
    }

    /**
     * 新增岗位
     */
    @PreAuthorize(hasPermi = "system:post:add")
    @PostMapping
    public Response<Void> add(@Validated @RequestBody SysPost post) {
        if (Constants.NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
            return Response.failed("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        } else if (Constants.NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
            return Response.failed("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
        post.setCreateBy(SecurityUtils.getUsername());
        postService.insertPost(post);
        return Response.success();
    }

    /**
     * 修改岗位
     */
    @PreAuthorize(hasPermi = "system:post:edit")
    @PutMapping
    public Response<Void> edit(@Validated @RequestBody SysPost post) {
        if (Constants.NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
            return Response.failed("修改岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        } else if (Constants.NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
            return Response.failed("修改岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
        post.setUpdateBy(SecurityUtils.getUsername());
        postService.updatePost(post);
        return Response.success();
    }

    /**
     * 删除岗位
     */
    @PreAuthorize(hasPermi = "system:post:remove")
    @DeleteMapping("/{postIds}")
    public Response<Void> remove(@PathVariable Long[] postIds) {
        postService.deletePostByIds(postIds);
        return Response.success();
    }

    /**
     * 获取岗位选择框列表
     */
    @GetMapping("/optionselect")
    public Response<List<SysPost>> optionselect() {
        List<SysPost> posts = postService.selectPostAll();
        return Response.success(posts);
    }
}
