package org.light4admin.configuration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author linzetian
 * @date 2021/4/30
 */
@Controller
public class WebConfiguration {
    public static final String RESOURCE_PATH = "classpath:/light4admin-web/";

    @GetMapping({"login","index"})
    private String index() {
        return "redirect:./index.html";
    }

}
