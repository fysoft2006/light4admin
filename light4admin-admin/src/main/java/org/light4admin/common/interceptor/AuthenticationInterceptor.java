package org.light4admin.common.interceptor;

import com.alibaba.fastjson.JSONObject;
import org.light4admin.common.CacheConstants;
import org.light4admin.common.Constants;
import org.light4admin.common.base.BusinessException;
import org.light4admin.common.base.ResponseCode;
import org.light4admin.modules.system.service.impl.RedisService;
import org.light4admin.modules.system.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author jinbin
 * @date 2018-07-08 20:41
 */
@Component
public class AuthenticationInterceptor implements HandlerInterceptor {
    @Value("${token.enabled:true}")
    private Boolean enabled;

    @Resource(name = "stringRedisTemplate")
    private ValueOperations<String, String> sops;
    private final static long EXPIRE_TIME = Constants.TOKEN_EXPIRE * 60;
    @Autowired
    private RedisService redisService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object object) throws Exception {
        // 如果不是映射到方法直接通过
        if (!enabled || !(object instanceof HandlerMethod)) {
            return true;
        }

        String token = SecurityUtils.getToken();
        if (StringUtils.isEmpty(token)) {
            throw new BusinessException(ResponseCode.UN_LOGIN,"token不能为空");
        }


        String userStr = sops.get(getTokenKey(token));
        if (StringUtils.isEmpty(userStr)) {
            throw new BusinessException(ResponseCode.UN_LOGIN,"登录状态已过期");
        }


        JSONObject obj = JSONObject.parseObject(userStr);
        String userid = obj.getString("userid");
        String username = obj.getString("username");

        if (StringUtils.isEmpty(userid) || StringUtils.isEmpty(username)) {
            throw new BusinessException(ResponseCode.UN_LOGIN,"令牌验证失败");
        }


        redisService.expire(getTokenKey(token), EXPIRE_TIME);

        request.setAttribute(CacheConstants.DETAILS_USER_ID, userid);
        request.setAttribute(CacheConstants.DETAILS_USERNAME, username);
        return true;

    }


    private String getTokenKey(String token) {
        return CacheConstants.LOGIN_TOKEN_KEY + token;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }


}
