package org.light4admin.common.base;

/**
 * 基础响应码
 *
 * @author light4admin
 * @Date: 2018-02-01 23:08
 */
public enum ResponseCode {
    SUCCESS(200, "操作成功"),
    FAILED(501, "操作失败"),
    SYS_EXCEPTION(500, "服务异常"),
    PARAM_ILLEGAL(400, "参数失败"),
    UN_LOGIN(401, "请先登录"),
    FORBIDDEN(403, "权限不足"),
    PLEASE_RETRY_VERIFY_TRANS(601, "验证次数超过限制，请重新发起打款"),
    VERIFY_TRANS_OVER_LIMIT(602, "多次校验失败，请联系客户经理"),

    ;

    private Integer code;

    private String message;

    ResponseCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
