package org.light4admin.common.base;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.light4admin.util.SqlUtil;

import java.util.List;
import java.util.Objects;

/**
 * web层通用数据处理
 *
 * @author light4admin
 */
public class BaseController {

    /**
     * 设置请求分页数据
     */
    protected void startPage() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (Objects.nonNull(pageNum) && Objects.nonNull(pageSize)) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected PageResponse getDataTable(List<?> list) {
        PageResponse rspData = new PageResponse();
        rspData.setCode(ResponseCode.SUCCESS.getCode());
        rspData.setRows(list);
        rspData.setMsg("获取成功");
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }
}
