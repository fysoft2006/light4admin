package org.light4admin.common.base;

/**
 * 基础异常类
 *
 * @author light4admin
 * @date 2019/6/26
 */
public class BusinessException extends RuntimeException {

    private Integer code;
    private String errorMessage;
    private Throwable cause;
    private Object data;

    private BusinessException() {
    }

    public BusinessException(String message) {
        this.setCode(ResponseCode.FAILED.getCode());
        this.setErrorMessage(message);
    }

    public BusinessException(ResponseCode code) {
        this.setCode(code.getCode());
        this.setErrorMessage(code.getMessage());
        this.cause = new Throwable(code.getMessage());
    }

    public BusinessException(String msg, Throwable e) {
        super(msg);
        this.setCode(ResponseCode.FAILED.getCode());
        this.setErrorMessage(msg);
        this.cause = e;
    }

    public BusinessException(ResponseCode businessCode, String errorMessage) {
        this.setCode(businessCode.getCode());
        this.setErrorMessage(errorMessage);
        this.cause = new Throwable(errorMessage);
    }

    public BusinessException(ResponseCode businessCode, String errorMessage, Object data) {
        this.setCode(businessCode.getCode());
        this.setErrorMessage(errorMessage);
        this.setData(data);
        this.cause = new Throwable(errorMessage);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }

    public synchronized void setCause(Throwable cause) {
        this.cause = cause;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
