package org.light4admin.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 基础分页请求基类
 *
 * @author light4admin
 * @date: 2018/8/22 7:15
 */
@Data
public class PageBaseRequest extends BaseRequest {
    private static final Integer MAX_PAGE_SIZE = 3000;

    @ApiModelProperty(value = "第几页")
    private Integer pageNum = 1;

    @ApiModelProperty(value = "获取的大小数")
    private Integer pageSize = 10;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize > MAX_PAGE_SIZE ? MAX_PAGE_SIZE : pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
