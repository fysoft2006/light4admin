package org.light4admin.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

import java.util.Objects;

/**
 * 通用返回对象
 *
 * @author light4admin
 * @date 2019/4/19
 */
@ToString
public class Response<T> {

    @ApiModelProperty("响应码")
    private Integer code;

    @ApiModelProperty("响应消息")
    private String msg;

    @ApiModelProperty("响应数据")
    private T data;


    /**
     * 成功返回结果
     */
    public static Response<Void> success() {
        return new Response<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), null);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> Response<T> success(T data) {
        return new Response<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data    获取的数据
     * @param message 提示信息
     */
    public static <T> Response<T> success(T data, String message) {
        return new Response<>(ResponseCode.SUCCESS.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param code 错误码
     */
    public static Response<Void> failed(ResponseCode code) {
        return new Response<>(code.getCode(), code.getMessage(), null);
    }

    /**
     * 失败返回结果
     *
     * @param code 错误码
     */
    public static <T> Response<T> failed(ResponseCode code, String message) {
        return new Response<>(code.getCode(), message, null);
    }

    /**
     * 失败返回结果
     *
     * @param code 错误码
     */
    public static <T> Response<T> failed(ResponseCode code, T data, String message) {
        return new Response<>(code.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> Response<T> failed(T data, String message) {
        return new Response<>(ResponseCode.FAILED.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> Response<T> failed(String message) {
        return new Response<>(ResponseCode.FAILED.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static Response<Void> failed() {
        return failed(ResponseCode.FAILED);
    }

    /**
     * 参数验证失败返回结果
     */
    public static Response<Void> validateFailed() {
        return failed(ResponseCode.PARAM_ILLEGAL);
    }

    /**
     * 参数验证失败返回结果
     *
     * @param message 提示信息
     */
    public static Response<Void> validateFailed(String message) {
        return new Response<>(ResponseCode.PARAM_ILLEGAL.getCode(), message, null);
    }

    /**
     * 判断是否成功
     *
     * @return
     */
    public static <T> boolean isSuccess(Response<T> response) {
        return Objects.nonNull(response) && ResponseCode.SUCCESS.getCode().equals(response.getCode());
    }

    public Response() {
    }

    public Response(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
