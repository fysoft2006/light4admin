package org.light4admin.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 基础请求基类
 *
 * @author light4admin
 * @date: 2018/8/22 7:15
 */
@ToString
@Data
public class BaseRequest {
    @ApiModelProperty(value = "经度", hidden = true)
    private String lng;

    @ApiModelProperty(value = "维度", hidden = true)
    private String lat;

    @ApiModelProperty(value = "设备型号", hidden = true)
    private String deviceType;

    @ApiModelProperty(value = "网络信息", hidden = true)
    private String networkInfo;

    @ApiModelProperty(value = "设备唯一id", hidden = true)
    private String udid;
}
