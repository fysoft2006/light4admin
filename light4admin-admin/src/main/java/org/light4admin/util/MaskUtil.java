package org.light4admin.util;

import org.springframework.util.StringUtils;

/**
 * 脱敏工具
 *
 * @author light4admin
 * @date 2021/4/9
 */
public class MaskUtil {
    private MaskUtil() {

    }

    /**
     * 对手机号加掩码
     *
     * @param phone 手机号
     * @return
     */
    public static String maskPhone(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return "";
        }
        return phone.substring(0, 4) + "****" + phone.substring(9);
    }

}
