package org.light4admin.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 环境工具类
 *
 * @author light4admin
 * @date: 2019/11/2 11:32
 */
@Component
public class Profile {
    @Value("${spring.profiles.active}")
    private String springProfilesActive;

    public boolean isProd() {
        return "pro".equalsIgnoreCase(springProfilesActive);
    }
}
