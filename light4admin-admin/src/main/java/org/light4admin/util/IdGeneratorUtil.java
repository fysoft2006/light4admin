package org.light4admin.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author nongdehua
 * @date 2019/8/31
 **/
@Component
@RefreshScope
public class IdGeneratorUtil implements InitializingBean {

    @Value("${id.generator.workerId:1}")
    private Integer workerId;
    @Value("${id.generator.datacenterId:2}")
    private Integer datacenterId;

    private Snowflake snowflake;

    @Override
    public void afterPropertiesSet() {
        snowflake = IdUtil.createSnowflake(workerId, datacenterId);
    }

    public Long getId() {
        return snowflake.nextId();
    }

}
