## 平台简介

light4admin是一个追求轻量、极简的spring后台管理脚手架。具备特点

* 后端采用spring-cloud-alibaba、Mybatis Plus、Redis、 nacos-config&discovery。
* 前端采用最新Vue、Element UI，前后端分离。
* 支持后台前端打包一起部署，真正实现一键启动
* 支持加载动态权限菜单，多方式轻松权限控制。

预览地址:http://47.98.128.127:8080/index.html
账号/密码：light4admin/light4admin

## 系统需求

* JDK >= 1.8
* MySQL >= 5.7
* Maven >= 3.0
* 最新 Redis
* nodejs & 安装cnpm

## 快速开始

- 1.根据脚本db/light4admin.sql 建好数据库并调整数据库账号密码
- 2.运行：org.light4admin.Application.main
- 3.启动成功打开:http://127.0.0.1:8080/index.html

注意：
  若首次打开项目，需要打包light4admin-web，只需进到light4admin-web根目录执行 mvn install 即可
## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。



## 演示图

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0502/002810_9ba2e894_516217.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0502/002942_5c36adbc_516217.jpeg"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0502/003129_e8e25361_516217.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0502/003219_50a84a82_516217.jpeg"/></td>
    </tr>

    
    
   
</table>

## 其他
[购买阿里云服务器特惠](https://www.aliyun.com/minisite/goods?userCode=lbm3vfce&share_source=copy_link)

## 交流 

qq群：661329224
